from Course.models import Level,Subject
from django.forms import ModelForm

def AddSubject(ModelForm):
    class Meta:
        model = Subject
        fields = '__all__'

def AddDegree(ModelForm):
    class Meta:
        model = Level
        fields = '__all__'

from django.shortcuts import render
from Course.forms import AddCourse,AddSchool
from Course.models import Institution,Level,Subject

from .forms import AddSubject,AddDegree


def Main(request):
    config = {
        'navbar': 'black',
        'footer': 'black'
    }
    return render(request ,'Main.html',config)

def formSchool(request):

    if(request.method == 'POST'):
        Form = AddSchool(request.POST)
        if(Form.isValid()):
            Form.save()

    config = {
        'navbar': 'black',
        'footer': 'black'
    }
    return render(request ,'formschool.html',config)


def formSubject(request):

    if(request.method == 'POST'):
        form = AddSubject(request.POST)
        if(form):
            form.save()

    form = AddSubject(instance=Subject)
    config = {
        'navbar': 'black',
        'footer': 'black',
        'forms' : AddSubject,
    }
    return render(request, 'formSubject.html',config)


def formDegree(request):
    config = {
        'navbar': 'black',
        'footer': 'black'
    }
    return render(request, 'formDegree.html',config)
from django.urls import path

from .views import Main,formSchool,formDegree,formSubject

urlpatterns = [
    path('', Main, name='Admins'),
    path('school/',formSchool),
    path('degree/', formDegree),
    path('subject/', formSubject),
]

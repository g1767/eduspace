from django.urls import path

from .views import FAQList,AddFAQ

urlpatterns = [
    path('', FAQList, name='FAQ'),
    path('add/',AddFAQ, name='add-faq')
]

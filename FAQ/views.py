from django.shortcuts import render
from django.http import HttpResponse

from .models import FAQ

def  FAQList(request):
    faqs = FAQ.objects.all()
    response  = {
        'faqs' : faqs,
    }
    return render(request, 'FAQ.html' , response)

def AddFAQ(request):
    print(request.POST)
    '''
    Todo :
    Lakukan save pada model models FAQ sesuai input pada request.post
    '''
    return render(request, 'AddFAQ.html')
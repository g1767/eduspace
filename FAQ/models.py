from django.db import models

# Create your models here.
class FAQ(models.Model):
    Question = models.TextField()
    Answer = models.TextField()

    def __str__(self):
        return self.Question
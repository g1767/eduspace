from django.shortcuts import render, HttpResponseRedirect, redirect
from django.http import HttpResponse, JsonResponse
from django.contrib.auth import login, logout, authenticate
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .forms import RegistrationForm
import time

def index(request):
    return render(request, 'Auth.html')

def registViews(request):
    if request.user.is_authenticated:
        return redirect('login')
    else:
        form = RegistrationForm()
        if request.method == 'POST':
            form = RegistrationForm(request.POST)
            UserValid = form.is_valid()
            if form.is_valid():
                form.save()
                return redirect('login')

        context = {'regist_form' : form}
        return render(request, 'register.html', context)

def loginViews(request):
    if request.user.is_authenticated:
        return redirect('dashboard')
    else:
        if request.method == 'POST':
            username = request.POST.get("username")
            password = request.POST.get("password")
            user = authenticate(request, username = username, password = password)
            if user is not None:
                login(request, user)
                return redirect('dashboard')
            else:
                messages.info(request, "Invalid Username or Password")
                return redirect('login')

        context = {}
        return render(request, 'login.html', context)

def logoutUser(request):
    logout(request)
    return redirect('Homepage')


def ret_json(request):
    t = time.localtime()
    curr_time = time.strftime("%H:%M:%S", t)
    return JsonResponse(curr_time, safe=False)

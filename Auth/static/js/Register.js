var form_fields = document.getElementsByTagName('input')

form_fields[1].placeholder='Username';
form_fields[2].placeholder='Email';
form_fields[3].placeholder='Enter password';
form_fields[4].placeholder='Confirm password';

for(let i = 1; i < 5; i++){
    form_fields[i].style.width = '560px';
    form_fields[i].style.height = '39px';
    form_fields[i].style.margin = '15px';
    form_fields[i].style.borderStyle = 'none none solid none';
    form_fields[i].style.borderWidth = '2px';
    form_fields[i].style.borderColor = '#000000';
}

for (var field in form_fields){	
    form_fields[field].className += ' form-control'
}

$(document).ready(function(){
    $.ajax({
        url: "returnJSON",
        dataType: 'json',
        success: function(data){
            console.log(data)
            $('#myTable').append($('<tr>')
                .append("Access time: " + data)
            )
        }
    });
});
from django.urls import path 
from .views import index, registViews, loginViews, logoutUser, ret_json

urlpatterns = [
    path('', index, name='index'),
    path('login/', loginViews, name='login'),
    path('register/', registViews, name='register'),
    path('logout/',logoutUser , name='logout'),
    path('login/returnJSON/', ret_json, name='ret_json'),
    path('register/returnJSON/', ret_json, name='ret_json'),
]

// All Functional 

function getCookie(c_name)
{
    if (document.cookie.length > 0)
    {
        c_start = document.cookie.indexOf(c_name + "=");
        if (c_start != -1)
        {
            c_start = c_start + c_name.length + 1;
            c_end = document.cookie.indexOf(";", c_start);
            if (c_end == -1) c_end = document.cookie.length;
            return unescape(document.cookie.substring(c_start,c_end));
        }
    }
    return "";
 }

 $(function () {
    $.ajaxSetup({
        headers: { "X-CSRFToken": getCookie("csrftoken") }
    });
});



// ----------------------------------------------------------------




var Level = document.getElementById('dropdownMenuButton1')
var Subject = document.getElementById('dropdownMenuButton2')
var Institution = document.getElementById('dropdownMenuButton3')


var SelectedLevel = ''
var SelectedSubject = ''
var SelectedSubject = ''

// ------------------------- Content Pages ------------------------

var Material = document.querySelector('.material')
var Comments = document.querySelector('.comments')
var Material_Tab = document.querySelector('.material-tab')
var Comments_Tab = document.querySelector('.comments-tab')



function handleMaterial(){
    Comments.classList.remove('active') 
    Material.classList.add('active') 

    Material_Tab.classList.remove('hidden')
    Comments_Tab.classList.add('hidden')
}

function handleComments(){
    Material.classList.remove('active') 
    Comments.classList.add('active') 

    Comments_Tab.classList.remove('hidden')
    Material_Tab.classList.add('hidden')
}



function videoChange(e){
    const id_video = e
    var Course_id = document.querySelector('.id-course').id
    var TitleVideo = document.querySelector('.title')
    var VideoShown  = document.querySelector('#video-style')
    $.ajax({

        type: 'GET',

        url: `/Course/content/${Course_id}/${id_video}/`,

        success: function (response) {
            const title = response.title
            const url = response.url

            console.log(url)
            
            TitleVideo.innerHTML = title
            VideoShown.src= url
            VideoShown.load();
            VideoShown.play();
        },

        error: function (response) {

            console.log(response)

        }

    })
}

function start(){
    var Course_id = document.querySelector('.id-course').id
    var TitleVideo = document.querySelector('.title')
    var VideoShown  = document.querySelector('#video-style')
    $.ajax({
        type: 'GET',

        url: `/Course/content/${Course_id}/start/`,

        success: function (response) {
            const title = response.title
            const url = response.url

            
            console.log('SUCCESS')

            
            TitleVideo.innerHTML = title
            VideoShown.src= url
            VideoShown.load();
            VideoShown.play();
        },

        error: function (response) {

            console.log('ERROR')
            console.log(response)

        }

    })
}


function AddScore(){
    var URL = window.location.href
    var Pattern = URL.split('/')
    var IdIndex = 0;
    var Course_id = document.querySelector('.id-course').id

    for (let i = 0 ; i < Pattern.length ; i ++) {
        if(Pattern[i] === 'content')
            IdIndex = (i + 1);
    }

    var IdVideo = Pattern[IdIndex]
    
    $.ajax({
        type: 'GET',

        url: `/Course/content/${Course_id}/addPoint/`,

        success: function (response) {
            console.log(response)
        },

        error: function (response) {

            console.log('ERROR')
            console.log('The Errors : ',response)

        }

    })
}


function RedirectQuiz(){
    const URL_NOW = window.location.href;
    localStorage.setItem('back', URL_NOW);
    const CliclElement = event.srcElement;
    const Id_Element = CliclElement.id;
    const Redirect_URL = window.location.origin + `/Quiz/${Id_Element}/`
    console.log(Redirect_URL);
    window.location.replace(Redirect_URL)
}


// -----------------------------------------

function handleOne(){
    localStorage.setItem('value',1)
}

function handleTwo(){
    localStorage.setItem('value',2)
}
function handleThree(){
    localStorage.setItem('value',3)
}

function handleFour(){
    localStorage.setItem('value',4)
}

function handleFive(){
    localStorage.setItem('value',5)
}

function handleRate(){
    const Values = localStorage.getItem('value')
    var Modal = document.querySelector('.Modal-Rating')
    var Course_id = document.querySelector('.id-course').id
    $.ajax({
        type: 'POST',

        url: `/Course/content/${Course_id}/addStar/`,

        data : {
            value : Values
        },

        success: function (response) {
            console.log(response)
            Modal.style.display = 'none'
        },

        error: function (response) {

            console.log('ERROR')
            console.log('The Errors : ',response)

        }

    })
}

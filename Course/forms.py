from .models import Course,Institution
from django.forms import ModelForm

def AddCourse(ModelForm):
    class Meta:
        model = Course
        fields = '__all__'

def AddSchool(ModelForm):
    class Meta:
        model = Institution
        fields = ['institution_name','degree','description']
        # widget = {
        #     'institution_name' : forms.TextInput({'class':'form-control'})
        # }

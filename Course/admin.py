from django.contrib import admin
from .models import Course,Institution,DummyUser,Level,Subject,Upload,UploadFragment
from Quiz.models import Kuis

class VideoFileCourse(admin.TabularInline):
    model = UploadFragment

class All_Video(admin.ModelAdmin):
    inlines = [VideoFileCourse]

class UploadVideo(admin.TabularInline):
    model = Upload

class Video(admin.ModelAdmin):
    inlines = [UploadVideo]

admin.site.register(Course, Video)
admin.site.register(Institution)
admin.site.register(Level)
admin.site.register(Subject)
admin.site.register(Upload,All_Video)
from django.shortcuts import render
from django.http import HttpResponse, HttpResponseNotFound
from django.core import serializers
import itertools
from django.http import JsonResponse
from .models import Course,Level,Subject,Institution,Upload,UploadFragment
from User.models import Account
from Quiz.models import Kuis
from django.views.decorators.csrf import csrf_exempt

def CourseList(request):
    All = Course.objects.all()
    
    level = Level.objects.all()
    subject = Subject.objects.all()
    institution = Institution.objects.all()
    filtring = 'all'

    response = {
        'navbar' : 'black',
        'courses' : All,
        'levels' : level,
        'subjects' : subject,
        'institutions' : institution,
        'filtring' : filtring,
        'total' : len(All),
        'footer' : 'white',
    }

    if(request.POST):
        institution_asked = request.POST.get('institution',None)
        level_asked = request.POST.get('level',None)
        subject_asked = request.POST.get('subject',None)
        filtring = 'sorted by'

        filtered = list()
        if(institution_asked != 'null'):
            for cc in All:
                print (cc.institution, cc.institution.institution_name == institution_asked)
                if(cc.institution.institution_name == institution_asked):
                    filtered.append(cc)     
            filtring += f' institution'
        else:
            filtered = All
        
        if(level_asked != 'null'):
            filtered = filter(lambda x: x.level.name == level_asked, filtered)
            filtring += f' level' 
        

        filtered = list(filtered)
        if(subject_asked != 'null'):
            filtered = filter(lambda x: x.subject.name == subject_asked, filtered)
            filtring += f' subject'

        filtered = list(filtered)
        total = len(filtered)

        response['courses'] = filtered
        response['filtring'] = filtring
        response['total'] = total
        return render(request, 'CourseList.html',response)

    
    return render(request, 'CourseList.html',response)


def Overview(request,course_id):
    specific_course = Course.objects.get(id_course=course_id)
    current_user = request.user
    is_enroll = False
    
    all_user = specific_course.participant.split(' ')
    if(current_user.username in all_user):
        is_enroll = True


    response = {
        'navbar' : 'white',
        'course' : specific_course,
        'is_enroll' : is_enroll,
    }

    return render(request, 'OverviewCourse.html',response)


def Content(request, course_id):
    specific_course = Course.objects.get(id_course=course_id)
    videos = Upload.objects.filter(course=specific_course)
    all_quiz = Kuis.objects.filter(course=specific_course)
    print(all_quiz)

    current_user = request.user
    is_enroll = False
    
    all_user = specific_course.participant.split(' ')
    if(not current_user.username in all_user):
        specific_course.participant += f' {current_user.username}'
        specific_course.save()


    response = {
        'navbar' : 'white',
        'course' : specific_course,
        'videos' : videos,
        'quizs' : all_quiz,
    }

    return render(request, 'Content.html',response)


def SlidingVideo(request, course_id, video_id):
    specific_course = Course.objects.get(id_course=course_id)
    videos = Upload.objects.filter(course=specific_course)
    video = videos.filter(orders=video_id)[0]
    print(video)

    files = UploadFragment.objects.filter(upload=video)[0]
    file_url = files.file.url
    video_name = video.title

    response = {
        'title' : video_name,
        'url' : file_url,
    }

    return JsonResponse(response)
        
def Starter(request, course_id):
    specific_course = Course.objects.get(id_course=course_id)
    videos = Upload.objects.filter(course=specific_course)
    video = videos[0]


    files = UploadFragment.objects.filter(upload=video)[0]
    file_url = files.file.url
    video_name = video.title

    response = {
        'title' : video_name,
        'url' : file_url,
    }

    return JsonResponse(response)

def AddPoint(request, course_id):
    current_user = request.user
    account_target = Account.objects.get(user=current_user)
    specific_course = Course.objects.get(id_course=course_id)

    account_target.score += specific_course.course_point
    account_target.save()


    response = {
        'res' : 'Success'
    }

    return JsonResponse(response)


@csrf_exempt
def AddStar(request, course_id):
    specific_course = Course.objects.get(id_course=course_id)
    value = request.POST['value']
    specific_course.rating += int(value)
    specific_course.save()

    response = {
        'res' : 'Success'
    }

    return JsonResponse(response)


def SchoolAndPartner(request):
    institution = Institution.objects.all()
    response = {
        'navbar' : 'black',
        'footer' : 'black',
        'all' : institution,
    }
    return render(request,'School.html', response)
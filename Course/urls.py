from django.urls import path

from .views import CourseList,Overview,Content,SlidingVideo,Starter,AddPoint,AddStar,SchoolAndPartner

urlpatterns = [
    path('', CourseList, name='course-list'),
    path('school/',SchoolAndPartner, name='school'),
    path('<str:course_id>/',Overview, name='overviewCourse'),
    path('content/<str:course_id>/',Content, name='CourseContent'),
    path('content/<str:course_id>/<int:video_id>/',SlidingVideo, name='slidingVideo'),
    path('content/<str:course_id>/start/',Starter, name='StarterResponse'),
    path('content/<str:course_id>/addPoint/',AddPoint, name='AddPoint'),
    path('content/<str:course_id>/addStar/',AddStar, name='AddStar'),
]

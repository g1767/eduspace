from django.db import models



class DummyUser(models.Model):
    name = models.CharField(max_length=100)
    point = models.IntegerField(null=True, blank=True)


    def __str__(self):
        return self.name


class Institution(models.Model):
    institution_name = models.CharField(max_length=100 ,unique=True)
    degree = models.CharField(max_length=100)
    picture = models.ImageField(upload_to='school/',null=True, blank=True)
    description = models.TextField(null=True, blank=True)
    

    def __str__(self):
        return self.institution_name

class Subject(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name

class Level(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class Course(models.Model):
    id_course = models.CharField(max_length=100, null=True, blank=True)
    course_name = models.CharField(max_length=100)
    level = models.ForeignKey(Level, on_delete=models.DO_NOTHING,null=True, blank=True)
    subject = models.ForeignKey(Subject, on_delete=models.DO_NOTHING ,null=True, blank=True)
    rating = models.IntegerField()
    institution = models.ForeignKey(Institution, on_delete=models.DO_NOTHING,null=True, blank=True)
    quiz_point = models.IntegerField()
    course_point = models.IntegerField()
    picture = models.ImageField(upload_to='photos/%Y/%m/%d/')
    participant = models.TextField(blank=True)
    subject = models.ForeignKey(Subject, on_delete=models.DO_NOTHING,null=True, blank=True)
    description = models.TextField(blank=True)


    def __str__(self):
        return self.course_name

    def Rating(self):
        all_participants = self.participant.split(' ')
        average = self.rating / len(all_participants)
        ans = f'{average}'
        return ans

    def Star(self):
        all_participants = self.participant.split(' ')
        average = self.rating // len(all_participants)
        average = average // 2
        return (average)


    def ParticipatFrequent(self):
        all_participants = self.participant.split(' ')
        return len(all_participants)

    def IsContain(self, username):
        all_user = self.participant.split(' ')
        if(username in all_user):
            return True
        return False


class Upload(models.Model):
    title = models.CharField(max_length = 30)
    thumbnail = models.ImageField(upload_to='photos/%Y/%m/%d/',null=True, blank=True)
    orders = models.IntegerField(null=True, blank=True, unique=True)
    course = models.ForeignKey(Course, null=True, blank=True,on_delete=models.CASCADE)
    materi = models.CharField(null=True, blank=True, max_length=100)

    class Meta:
        ordering = ['orders']

    def __str__(self):
       return f'{self.orders}. {self.title}-{self.course}'

class UploadFragment(models.Model):
    file = models.FileField()
    upload = models.ForeignKey(Upload, related_name="files", on_delete=models.CASCADE)
    
    def __str__(self):
        return f'Belongs : {self.upload.title}'








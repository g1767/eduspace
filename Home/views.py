from django.shortcuts import render
from FAQ.models import FAQ

def index(request):
    faqs = FAQ.objects.all()
    response = {
        'navbar' : 'white',
        'footer' : 'black',
        'faqs' : faqs,
    }
    return render(request, 'Home.html', response)
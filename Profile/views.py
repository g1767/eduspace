from django.shortcuts import render, redirect
from User.models import Account
from django.http import HttpResponse, JsonResponse
from django.conf import settings
from Course.models import Course
from .forms import EditForm
from django.contrib import messages
import os

navbar = 'black'

def index(request):
    All = Course.objects.all()
    username_user = request.user.username

    response = {
        'navbar':navbar,
        'courses': All,
        'footer' : 'black',
    }
    return render(request, 'Profile.html',response)

def editProfile(request, id_account):

    account = Account.objects.get(id=id_account)
    form = EditForm(instance=account)

    if request.method == 'POST':
        form = EditForm(request.POST, request.FILES ,instance=account)

        if form.is_valid:
            messages.success(request, "Profile Updated.")
            form.save()  
            return redirect('profile')
    
    context = {'form' : form}
    return render(request, 'editProfile.html',context)

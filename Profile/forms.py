from django.forms import ModelForm
from django import forms
from User.models import Account

class EditForm(ModelForm):
    class Meta:
        model = Account
        fields = ['name', 'imageProfile']

        widget = {
            'name' : forms.TextInput({'class':'form-control'})
        }
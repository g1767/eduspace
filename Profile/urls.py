from django.urls import path

from .views import index, editProfile

urlpatterns = [
    path('', index, name='profile'),
    path('edit/<str:id_account>', editProfile, name='editProfile')
]

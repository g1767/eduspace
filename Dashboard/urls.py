from django.urls import path

from Dashboard.views import Dashboard

urlpatterns = [
    path('', Dashboard, name='dashboard'),
]
from django.shortcuts import render
from User.models import Account
from Course.models import Course

def Dashboard(request):
    current_user = request.user
    account = ''
    print(request.user.is_authenticated)
    if(current_user.is_authenticated and request.user.username != 'admin'):
        current_user = request.user
        account = Account.objects.get(user=current_user)
    myusername = current_user.username
    print('username : ',myusername)
    All_user = Account.objects.all()
    All_Course = Course.objects.all()
    Default = []
    course_user = []
    print(All_user)
    for i in All_Course :
        if(i.IsContain(myusername)):
            course_user.append(i)
    if len(All_Course) > 0 :
        Default = All_Course[0]

    filtered_user = []

    for i in All_user:
        if(i.user.username != 'admin'):
            filtered_user.append(i)

    if(len(filtered_user) > 10):
        filtered_user = filtered_user[:10]

    response = {
        'accounts' : filtered_user,
        'user' : account,
        'courses' : All_Course,
        'Default' : Default, 
        'course_user' : course_user,   
    }
    return render(request, 'Dashboard.html', response)




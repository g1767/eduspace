from django.contrib import admin
from .models import Kuis, Soal, Answer, Result
class Jawabannya(admin.TabularInline):
    model = Answer

class Soalnya(admin.ModelAdmin):
    inlines = [Jawabannya]

admin.site.register(Kuis)
admin.site.register(Soal, Soalnya)
admin.site.register(Answer)
admin.site.register(Result)
# Register your models here.

from django.shortcuts import render
from .models import Kuis, Soal, Answer, Result
from django.views.generic import ListView
from django.http import JsonResponse
#Refrensi https://dev.to/chukslord1/building-a-quiz-app-with-django-and-fauna-2ojo
#Refrensi https://medium.com/swlh/overview-building-a-full-stack-quiz-app-with-django-and-react-57fd07449e2f

from .models import Kuis, Soal, Answer, Result
from json import dumps
from django.http import JsonResponse

class KuisListView(ListView):
    model = Kuis
    template_name = 'Quiz.html'

def viewkuis(request, pk):
    kuis = Kuis.objects.get(pk=pk)
    return render(request, 'Kuisnya.html', {'obj':kuis})

def kuis_data_view(request, pk):
    kuis = Kuis.objects.get(pk=pk)
    soal = []
    for q in kuis.get_questions():
        jawaban = []
        for a in q.get_answers():
            jawaban.append(a.text)
        soal.append({str(q): jawaban})
    return JsonResponse({
        'data': soal,
    })

def save_kuis_view(request, pk):
    if request.is_ajax():
        soal2 = []
        data = request.POST
        data_ = dict(data.lists())
        data_.pop('csrfmiddlewaretoken')
        for k in data_.keys():
            soal = Soal.objects.get(text=k)
            soal2.append(soal)
        user = request.user
        kuis = Kuis.objects.get(pk=pk)

        score = 0
        multiply = 100/ kuis.banyaksoal
        results = []
        correct_answer = None

        for q in soal2:
            a_select = request.POST.get(q.text)
            if (a_select != ""):
                jawaban_soal = Answer.objects.filter(tanya=q)
                for a in jawaban_soal:
                    if (a_select == a.text):
                        if a.betul:
                            score+=1
                            correct_answer = a.text
                        else:
                            if a.betul:
                                correct_answer = a.text
                results.append({str(q): {'correct_answer': correct_answer, 'answered': a_select}})
            else:
                results.append({str(q): 'not answered'})
        score_ = score * multiply
        Result.objects.create(kuis=kuis, user=user, score=score_)

        if (score_ >= kuis.lulus):
            return JsonResponse({'pass':True, 'score':score_, 'results': results})
        else:
            return JsonResponse({'pass':False,'score':score_, 'results': results})


def PreQuiz(request, pk):
    CurrentQuiz = Kuis.objects.get(pk=pk)
    print(CurrentQuiz)
    response = {
        'navbar':'white',
        'footer' :'black',
        'namakuis' : CurrentQuiz.name,
    }
    return render(request, 'PreQuiz.html',response)

def LiveQuiz(request, pk, soal):
    CurrentQuiz = Kuis.objects.get(pk=pk)

    All_Soal = Soal.objects.all()
    CurrentSoal = []
    for i in All_Soal:
        if(i.quiz == CurrentQuiz):
            CurrentSoal.append(i)
    
    if(int(soal) > int(CurrentQuiz.banyaksoal)):
        response = {
            'navbar':'white',
            'footer' :'black',
            'lenSoal' : len(CurrentSoal),
         }
        return render(request, 'QuizFinish.html',response)

    

    indexes = int(int(soal)- 1)
    soal_now = CurrentSoal[indexes]
    soal_description = f'No {soal} of {CurrentQuiz.banyaksoal}'
    All_Answer = Answer.objects.all()
    CurrentAnswer = []

    for i in All_Answer:
        if(i.tanya == soal_now):
            CurrentAnswer.append(i)
    print(CurrentAnswer)
    response = {
        'navbar':'white',
        'footer' :'black',
        'namakuis' : CurrentQuiz.name,
        'numbering' : soal_description,
        'soal' : soal_now,
        'answers' : CurrentAnswer,
    }

    return render(request, 'LiveQuiz.html',response)


def AddQuizPoint(request):
    print(request.POST)
    return JsonResponse(request,{
        'Succes': 'ok'
    })


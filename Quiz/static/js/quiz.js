const modalBtns = [...document.getElementsByClassName('modal-button')]
const modalBody = document.getElementById('modal-body-confirm')
const startBtn = document.getElementById('start-button')

modalBtns.forEach(modalBtns=> modalBtns.addEventListener('click', ()=> {
    const pk = modalBtns.getAttribute('data-pk')
    const name = modalBtns.getAttribute('data-quiz')
    const soal = modalBtns.getAttribute('data-question')
    const lulus = modalBtns.getAttribute('data-lulus')

    modalBody.innerHTML = `
        <div class="h5 mb-3">Siap? "<b>${name}</b>"></div>
        <div class="text-muted>
            <ul>
                <li>level: <b>${soal}</b></li>
                <li>level: <b>${lulus}</b></li>
            </ul>
        </div>
    `
    
    startBtn.addEventListener('click', ()=>{
        window.location.href = url + pk
    })
}))
const url = window.location.href
const quizBox = document.getElementById('box-kuis')
const scoreBox = document.getElementById('scoresc')
const resultBox = document.getElementById('resultsc')

$.ajax({
    type: 'GET',
    url: `${url}data`,
    success: function(response){
        //console.log(response)
        const data = response.data
        data.forEach(el => {
            for (const [soal, jawaban] of Object.entries(el)){
                quizBox.innerHTML +=`
                    <hr>
                    <div class="mb-2">
                        <b>${soal}</b>
                    </div>
                `
                jawaban.forEach(jawab=>{
                    quizBox.innerHTML +=`
                        <div>
                            <input type="radio" class="ans" id="${soal}-${jawab}" name"${soal}" value="${jawab}">
                            <label for="${soal}">${jawab}</label>
                    `
                })
            }
        });
    },
    error: function(error){
        console.error()
    }
})

const quizForm = document.getElementById('form-kuis')
const csrf = document.getElementsByClassName('csrfmiddlewaretoken')

const sendData = () => {
    const elements = [...document.getElementsByClassName('ans')]
    const data = {}
    data['csrfmiddlewaretoken'] = csrf[0].value
    elements.forEach(el=> {
        if (el.checked) {
            data[el.name] = el.value
        }
        else {
            if (!data[el.name]) {
                data[el.name] = null
            }
        }
    })

    $.ajax({
        type: 'POST',
        url: `${url}save/`,
        data: data,
        success: function(response){
            const results = response.results
            console.log(results)
            quizForm.classList.add('not-visible')

            scoreBox.innerHTML = `${response.pass ? 'Selamat!' : 'Tetap berusaha :) '} Hasil ${response.score.toFixed(2)}%`
            results.forEach(res=>{
                const resDiv = document.createElement("div")
                for (const [question, resp] of Object.entries(res)){
                    resDiv.innerHTML += question
                    const cls = ['container', 'p-3', 'text-light', 'h3']
                    resDiv.classList.add(...cls)

                    if (resp=='not answered'){
                        resDiv.innerHTML += '- not answered'
                        resDiv.classList.add('bg-danger')
                
                    }
                    else {
                        const answer = resp['answered']
                        const correct = resp['correct_answer']
                        
                        if (answer==correct){
                            resDiv.classList.add['bg-success']
                            resDiv.innerHTML += `answered: ${answer}`
                        }
                        else {
                            resDiv.classList.ad('bg-danger')
                            resDiv.innerHTML += `| correct answer: ${correct}`
                            resDiv.innerHTML += `| answered: ${answer}`
                        
                        }
                    }
                }
                resultBox.append(resDiv)
            })

        },
        error: function(error){
            console.log(error)
        }
    })
}
quizForm.addEventListener('submit', e=>{
    e.preventDefault

    sendData()
})

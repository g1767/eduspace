from django.urls import path
from .views import KuisListView, viewkuis, kuis_data_view, save_kuis_view, PreQuiz , LiveQuiz, AddQuizPoint

app_name = 'Quiz'

urlpatterns = [
    path('', KuisListView.as_view(), name='halaman-kuis'),
    path('<pk>/',PreQuiz ,name='pre-quiz'),
    path('<pk>/save/', save_kuis_view, name='save-view'),
    path('<pk>/data/', kuis_data_view, name='kuis-data-view'),
    path('<pk>/doing/<soal>/', LiveQuiz, name='live-quiz'),
    path('addPoint/', AddQuizPoint, name='add-quiz-point'),
]

from django.db import models
from User.models import Account
from Course.models import Course
class Kuis(models.Model):
    name = models.CharField(max_length=120)
    topic = models.CharField(max_length=120)
    banyaksoal = models.IntegerField(default=1)
    lulus = models.IntegerField(help_text="Score minimal lulus %")
    course = models.ForeignKey(Course, on_delete=models.DO_NOTHING, null=True, blank=True)
    
    def __str__(self):
        return f"{self.name}-{self.topic} - {self.pk}"

    def get_questions(self):
        return self.soal_set.all()[:self.banyaksoal]
    
    class Meta:
        verbose_name_plural = 'Kuisis'


class Soal(models.Model):
    text = models.CharField(max_length=200)
    quiz = models.ForeignKey(Kuis, on_delete=models.CASCADE)
    dibuat = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'{str(self.text)} - {self.pk}'
    
    def getanswer(self):
        return self.jawaban.all()

class Answer(models.Model):
    text = models.CharField(max_length=200)
    betul = models.BooleanField(default=False)
    tanya = models.ForeignKey(Soal, on_delete=models.CASCADE, related_name='jawaban')
    dibuat = models.DateTimeField(auto_now_add=True)

    def __str__(self) -> str:
        return f"pertanyaan: {self.tanya.text}, jawaban:{self.text}, benar: {self.betul}"

class Result(models.Model):
    quiz = models.ForeignKey(Kuis, on_delete=models.CASCADE)
    user = models.ForeignKey(Account, on_delete=models.CASCADE)
    score = models.FloatField()

    def __str__(self):
        return str(self.pk)

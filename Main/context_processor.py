from User.models import Account


def code_base(request):
    if(request.user.is_authenticated and request.user.username != 'admin'):
        current_user = request.user
        account = Account.objects.get(user=current_user)
        return {'account':account}
    return {
        'NoAuth': 'Yes'
    }

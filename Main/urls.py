"""Main URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""

from django.contrib import admin
from django.urls import path,include
from django.conf import settings
from django.conf.urls.static import static
import Home.urls as Home
import Course.urls as Course
import FAQ.urls as FAQ
import Auth.urls as Auth
import Dashboard.urls as Dashboard
import Profile.urls as Profile
import Quiz.urls as Quiz
import Example.urls as Examples
import admins.urls as admins

urlpatterns = [
    path('admin/', admin.site.urls),
    path('',include(Home)),
    path('Course/',include(Course)),
    path('FAQ/',include(FAQ)),
    path('Dashboard/',include(Dashboard)),
    path('Profile/',include(Profile)),
    path('Quiz/',include(Quiz)),
    path('Auth/',include(Auth)),
    path('Example/',include(Examples)),
    path('Only/Admin/',include(admins)),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

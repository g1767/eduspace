from django.db import models
from django.contrib import auth
from django.contrib.auth.hashers import (
    check_password, is_password_usable, make_password,
)
from django.contrib.auth.signals import user_logged_in
from django.contrib.contenttypes.models import ContentType
from django.core import validators
from django.core.exceptions import PermissionDenied
from django.core.mail import send_mail
from django.db import models
from django.db.models.manager import EmptyManager
from django.utils.crypto import get_random_string, salted_hmac
from six import python_2_unicode_compatible
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from Course.models import Course


# Account for user
class Account(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    name = models.CharField(max_length=30, null=True, blank=True)
    score = models.IntegerField(default=0,null=True, blank=True)
    imageProfile = models.ImageField(upload_to='photos/%Y/%m/%d',null=True, blank=True)

    def __str__(self):
        return self.user.username;

    def getProfilPicture(self):
        if(self.imageProfile):
            return self.imageProfile.url;
        return "https://img2.pngdownload.id/20180714/fok/kisspng-computer-icons-question-mark-clip-art-profile-picture-icon-5b49de29708b76.026875621531567657461.jpg"



@receiver(post_save, sender=User)
def update_profile_signal(sender, instance, created, **kwargs):
    if created:     
        Account.objects.create(user=instance)




